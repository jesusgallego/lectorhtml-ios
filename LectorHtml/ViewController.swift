//
//  ViewController.swift
//  LectorHtml
//
//  Created by Antonio Pertusa on 26/10/16.
//  Copyright © 2016 Antonio Pertusa. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var actionButton: UIBarButtonItem!
    
    @IBAction func connectUrl(_ sender: Any) {
        
        self.textField.resignFirstResponder() // Para ocultar el teclado
    
        // TODO: Realizar la conexión y mostrar el resultado de la url
        
        // 1. Crear la sesión
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        // 2. Crear URL para conectar a self.textField.text
        let urlString = self.textField.text
        
        // 3- Codificar la URL
        if let encodedString = urlString?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) {
            
            // 4. Crear petición a la URL
            let url = URL(string: encodedString)
            let request = URLRequest(url: url!)
            
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            actionButton.isEnabled = false
            // 5. Establecer conexión
            session.dataTask(with: request) { data, response, error in
                if let error = error {
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    self.actionButton.isEnabled = true
                    print(error.localizedDescription)
                } else {
                    let res = response as! HTTPURLResponse
                    
                    if res.statusCode == 200 {
                        
                        // 6. Guardar los resultados en el textView (si el texto es muy largo puede que no aparezca, pero no es un problema)
                        DispatchQueue.main.async {
                            UIApplication.shared.isNetworkActivityIndicatorVisible = false
                            self.actionButton.isEnabled = true
                            let content = String(data: data!, encoding: .ascii)!
                            print(content)
                            self.textView.text = content
                        }
                    } else {
                        print("Received status code: \(res.statusCode)")
                    }
                }
            }.resume()
        }
    }
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

